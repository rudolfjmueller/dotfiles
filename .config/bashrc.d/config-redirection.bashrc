alias isync='isync --config ~/.config/isync/config'
alias mbsync='mbsync --config ~/.config/isync/config'

alias tmux='tmux -f $XDG_CONFIG_HOME/tmux/tmux.conf'
