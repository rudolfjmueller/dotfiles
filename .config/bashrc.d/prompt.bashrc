if command -v starship &> /dev/null ; then
    eval "$(starship init bash)"
else
    parse_git_branch() {
         git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
    }
    PS1=$PS1"\[\e[91m\]\$(parse_git_branch)\[\e[00m\]"
    if [ -n "${NIX_GCROOT}" ]; then
        PS1="\[\e[31m\]<< nix >>\[\e[00m\]"$PS1
    fi
    export PS1=$PS1"\n>$ "
fi
