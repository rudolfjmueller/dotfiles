alias hosts='sed -n -e "/Host / s/\(Host\)\? \([.a-z0-9-]\+\)/\2\n/gp" ~/.ssh/config | sed "/^$/d" |fzy'
alias rebase-interactive='git log --oneline --decorate=short |fzy -p "select base:"| sed "s/^\([a-z0-9]\+\) .*$/\1/"| xargs git rebase -i'
alias vifm='vifmrun'
alias en='~/.scripts/execute-and-notify.sh'
alias t='/usr/bin/time -f "%E"'
alias openssl-encrypt='openssl enc -e -pbkdf2 -aes-256-cbc'
alias openssl-decrypt='openssl enc -d -pbkdf2 -aes-256-cbc'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

export IMAGE_PATH=~/.local/share/rhebo/rip
alias li='ls -alh ${IMAGE_PATH}'

alias git-fetch='git fetch --all -p && git fetch --prune origin +refs/tags/*:refs/tags/*'

# Timetracking stuff
## create a new stamp
alias stempel='sh ~/timetracking/scripts/list-stamps.sh'

alias callme='sh ~/.scripts/rename-terminal.sh'
