call plug#begin('~/.local/share/site/plugged')
Plug 'vimwiki/vimwiki'
Plug 'Yggdroot/indentLine'
Plug 'elzr/vim-json'
Plug 'dense-analysis/ale'
Plug 'jamessan/vim-gnupg'
Plug 'aliou/bats.vim'
call plug#end()

let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

set t_Co=256

set cursorline
set colorcolumn=80

syntax on

"remove useless and stupid conceal from json
let g:vim_json_syntax_conceal = 0

" ignore casing in searches but respect uppercase
set ignorecase
set smartcase

" ask before closeing modified buffers
set confirm

" Autocompletion
set wildmode=longest,list,full

" split options
set splitbelow splitright

" enable system clipboard
set clipboard+=unnamedplus

" configure linting
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'

" load all configurations from sections
runtime! sections/*.vim

set exrc    
set secure
