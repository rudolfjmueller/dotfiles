" spell check settings

set spelllang=en,de,cjk

nnoremap <silent> <Leader>o :set spell!<CR>
inoremap <silent> <F11><Leader>o:set spell!<CR>
