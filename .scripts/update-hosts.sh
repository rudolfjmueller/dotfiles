#!/bin/sh
sed '/^#Rhebo/,$d' /etc/hosts > /tmp/hosts
echo "#Rhebo hosts" >> /tmp/hosts

curl --silent -X GET http://inventory.rhebo:8080/api/inventories | jq -c '.[] | {name:.deviceName,ip:.ip}'| sed -n '/ /!p' | sed -E 's/^.*:"(.*)","ip":"(.*)"}$/\2 \1/' >> /tmp/hosts

sudo mv /tmp/hosts /etc/hosts

