#!/bin/sh

SCRIPT_PATH="$HOME/.scripts"
SELECTED_SCRIPT=$(ls -1 "${SCRIPT_PATH}/" | grep -v rs.sh | fzy)

sh -c "${SCRIPT_PATH}/${SELECTED_SCRIPT} $@"
