#!/bin/sh
git worktree remove $(git worktree list | grep -v $(pwd) | sed 's/^\([^ ]\+\).*$/\1/' | fzy) --force
