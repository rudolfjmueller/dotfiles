#!/bin/sh

usage() {
    echo "Usage: $(basename ${0}) [COMMAND]
    man    Request for a topic and display the related man page in a new terminal window
    htop   Open HTOP in a new terminal window
    wiki   Open Vim Wiki in a new terminal window
    notes  Open an empty NVIM instance in a new terminal window
    "
    return 0
}

case "$1" in
    man)
        export MANPAGER='nvim +Man!'
        apropos . | \
            rofi -dmenu | \
            awk '{print $1}' | \
            xargs -I% alacritty -t "Help for %" -e man %
        ;;
    htop)
        alacritty -t htop -e htop
        ;;
    wiki)
        alacritty -t Wiki -e nvim /home/rudolf/vimwiki/index.md
        ;;
    notes)
        alacritty -t Notizen -e nvim
        ;;
    help)
        usage;;
    --help)
        usage;;
    *)
        echo "don't know subcommand '${1}'."
        echo
        usage
        exit 1
esac
