#!/bin/sh

curl --silent --globoff --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/todos?state=pending&per_page=100" |jq -c '.[]|select((.target.state=="closed") or (.target.state=="merged"))|.id' | xargs -I{} curl --silent --request POST --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/todos/{}/mark_as_done" |jq -c '{"ID": .id, "state": .state}'
