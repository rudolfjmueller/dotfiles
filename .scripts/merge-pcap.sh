#!/bin/bash
shopt -s extglob

UUID=$(uuidgen)

echo "fixing corrupt files..."

for file in *pcap?(ng); do
    tshark -r "$file" -w "$file".fixed
    mv "$file".fixed "$file"
done

echo "start merging..."
i=0
opfiles=()
while read threefiles; do
    mergecap -w "$UUID"-$i.pcap.tmp $threefiles
    opfiles+=("$UUID-$i.pcap.tmp")
    ((i++)) 
done < <(echo *pcap?(ng) | xargs -n 1000)
mergecap -w merged-"$UUID".pcapng "${opfiles[@]}"
rm "${opfiles[@]}"
