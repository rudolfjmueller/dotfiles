#!/bin/sh

PORT_SELECTION=$(echo 'Swagger:80:8081\nKibana:5601:5602\nDatabase:9200:9201' | fzy)
REMOTE_PORT=$(echo "${PORT_SELECTION}" | sed 's/^.*:\([0-9]\+\):\([0-9]\+\)/\1/')
LOCAL_PORT=$(echo "${PORT_SELECTION}" | sed 's/^.*:\([0-9]\+\):\([0-9]\+\)/\2/')
TARGET=$(echo 'moonshine\nsunshine' | fzy)

echo "Forwarding local port ${LOCAL_PORT} to port ${REMOTE_PORT} on ${TARGET}..."
ssh -NL ${LOCAL_PORT}:localhost:${REMOTE_PORT} ${TARGET}
