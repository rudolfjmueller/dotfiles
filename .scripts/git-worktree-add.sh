#!/bin/sh
git fetch --all -p
BRANCH=$(git branch --all | grep -v "^* " | grep -v "origin/HEAD" | sed 's/^  \(remotes\/\)\?//g' | fzy | sed 's/\(origin\/\)\?//g')
git worktree add "../${BRANCH}" "${BRANCH}"
