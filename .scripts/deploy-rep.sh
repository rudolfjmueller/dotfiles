#!/bin/sh

TARGET=130.211.49.205
USER=rudolf.mueller
export TAG=$(git branch --show-current)
ssh -i ~/.ssh/id_rhebo -N -L /tmp/sonnen-remote-docker.sock:/var/run/docker.sock "${USER}"@"${TARGET}" &

SSH_TUNNEL_PID=$!
export DOCKER_HOST=unix:///tmp/sonnen-remote-docker.sock
docker login --username "${RHEBO_GITLAB_USER}" --password $(pass gitlab) "${RHEBO_GITLAB_REGISTRY}"
scripts/create-zabbix-agent-override-file.sh "${TARGET}" zabbix.sonnenbatterie.de
cd infrastructure/
docker-compose -f docker-compose.yaml pull
docker-compose -f docker-compose.yaml -f docker-compose.es-data.yaml -f docker-compose.api-prod.yaml -f docker-compose.zabbix-agent.yaml -f docker-compose.es-memory.yaml up --detach --remove-orphans
docker image prune --force --all
kill "${SSH_TUNNEL_PID}" && wait "${SSH_TUNNEL_PID}"
