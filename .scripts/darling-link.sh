#!/bin/sh

DIRECTORY=$(pwd)
DARLING_DIR=~/.local/share/rhebo/darling/

SELECTED_DB=$(ls -1 ${DARLING_DIR} |fzy)

SOURCE_DB=${DARLING_DIR}${SELECTED_DB}
DESTINATION_DB=${DIRECTORY}/darling.db

rm -f ${DESTINATION_DB}
ln -s ${SOURCE_DB} ${DESTINATION_DB}
