#!/bin/sh

i=1
while :
do
    PIPELINES=$(curl --silent --globoff --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/12393946/pipelines?per_page=100&page=$i" | jq '.[]|.id')
    if [ -n "$PIPELINES" ]
    then
        echo "$PIPELINES" | while read PIPELINE ; do
            if [ -n "$PIPELINE" ]
            then
                SHIPPING=$(curl --silent --globoff --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/12393946/pipelines/$PIPELINE/jobs?scope[]=manual&scope[]=success" | jq -c '.[]|select( .stage == "deploy")| select( .started_at != null)|{name:.name,start:.started_at,finish: .finished_at, user: .user.username, title: .commit.title}')
                if [ -n "$SHIPPING" ]
                then
                    echo "$SHIPPING,"
                fi
            fi
        done
    else
        break
    fi
    i=$((i+1))
done
