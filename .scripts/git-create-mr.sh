#!/bin/sh
TMP_FILE="$XDG_RUNTIME_DIR"/mr.txt
git fetch --all -p
TARGET=$(git branch -r | grep -v "origin/HEAD" | fzy | sed 's/\s*origin\///g')
SOURCE=$(git branch | sed -n -e 's/^* \(.*\)$/\1/gp')

printf "# Merge %s in %s\n# !up creates upstream branch (containing lines are deleted)\n# !force executes a forced push (containing lines are deleted)\n# lines that start with \"#\" are ignored" "${SOURCE}" "${TARGET}" > "${TMP_FILE}"
nvim -n "${TMP_FILE}"
TITLE=$(sed -n 1p "${TMP_FILE}")
sed 1d "${TMP_FILE}"

OPTIONS=
if grep -q "!force" "${TMP_FILE}" ; then
    OPTIONS="${OPTIONS} --force-with-lease"
fi
if grep -q "!up" "${TMP_FILE}" ; then
    OPTIONS="${OPTIONS} --set-upstream origin ${SOURCE}"
fi
echo $OPTIONS
DESCRIPTION=$( (sed '/^#/d' | sed '/!up/d' | sed '/!force/d' | sed ':a;N;$!ba;s/\n/\\n/g' | sed 's/"/\\"/g') < "${TMP_FILE}")

CREATION_FLAGS="-o merge_request.create -o merge_request.remove_source_branch"

#git push ${CREATION_FLAGS} -o merge_request.target="${TARGET}" -o merge_request.title="${TITLE}" -o merge_request.description="${DESCRIPTION}" ${OPTIONS}
