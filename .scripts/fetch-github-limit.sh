#!/bin/sh

RATE=$(curl --silent -u "$RHEBO_GITHUB_USER":"$RHEBO_GITHUB_TOKEN" -H "Accept: application/vnd.github.v3+json" https://api.github.com/rate_limit | jq '.rate')
REMAINING=$(echo "$RATE" | jq '.remaining')
RESET=$(date -d@$(echo "$RATE" | jq '.reset'))

echo "Remaining access: $REMAINING"
echo "Count reset at: $RESET"
