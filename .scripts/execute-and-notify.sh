#!/bin/sh

COMMAND=$@
RESULT=

if sh -c "${COMMAND}" ; then
    RESULT="successfully"
else
    RESULT="faulty"
fi

notify-send "${COMMAND}" "Executed ${RESULT}"
