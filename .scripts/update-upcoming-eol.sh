#!/bin/sh

PARAMETER="$1"
if [ "${PARAMETER}" = "--notify" ] ; then
    NOTIFY=1
fi
if [ -z "${RHEBO_GITLAB_TOKEN+x}" ]; then
    RHEBO_GITLAB_TOKEN=$(grep RHEBO_GITLAB_TOKEN .config/bashrc.d/gitlab.token.bashrc | sed 's/^export .*="\(.*\)"$/\1/g')
fi

DATES=$(curl --silent --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/groups/rhebo/epics/3" | jq '.description'|sed 's/"//g' | sed 's/\\n/\n/g' | grep -E "^- [0-9]" | sed 's/- \([0-9]\{4\}\)\.\([0-9]\{2\}\)\.\([0-9]\{2\}\)/\1-\2-\3/g')

CUR_DATE=$(date '+%s')
NOTIFY_BODY=$(echo "${DATES}" | while IFS=':' read DATE TITLE ; do
    if [ -n "$DATE" ]
    then
        DATE_LIMIT=$((CUR_DATE+7686000))
        DATE_INT=$(date -d "${DATE}" '+%s')
        if [ "${CUR_DATE}" -lt "${DATE_INT}" ] && [ ${DATE_LIMIT} -ge "${DATE_INT}" ] ; then
            echo "${TITLE} is due at ${DATE} !!"
        fi
    fi
done)

if [ -n "${NOTIFY+1}" ]; then
    notify-send "Upcoming Deadline" "${NOTIFY_BODY}"
else
    echo "${NOTIFY_BODY}"
fi
