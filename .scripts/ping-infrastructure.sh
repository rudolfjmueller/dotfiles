#!/bin/sh
PARAMETER="$1"
if [ "${PARAMETER}" = "--notify" ] ; then
    NOTIFY=1
fi

STATUS=$(
    HOSTS=$(cat /etc/hosts |sed '1,/^#Rhebo/d' |sed 's#^.*\s##')
    if ping -q -c 4 -w 5 rhebo-nas-1 1>/dev/null; then
        for HOST in $HOSTS; do
            if ping -q -c 4 -w 5 "${HOST}" 1>/dev/null; then
                if [ -z "${NOTIFY}" ]; then
                    echo "${HOST} is available"
                fi
            else
                if [ -z "${NOTIFY}" ]; then
                    echo "\033[0;31m${HOST} is missing\033[0m"
                else
                    echo "${HOST} is missing"
                fi
            fi
        done
    fi
)

if [ -n "${NOTIFY+1}" ]; then
    if [ -n "$STATUS" ]; then
        notify-send -u critical "Infrastructure Status" "$STATUS"
    fi
else
    echo "$STATUS"
fi
