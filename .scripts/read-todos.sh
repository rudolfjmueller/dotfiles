#!/bin/sh

curl --globoff --header "PRIVATE-TOKEN: $RHEBO_GITLAB_TOKEN" "https://gitlab.com/api/v4/todos?state=pending" |jq -c '.[]|{title: .target.title, name:.body, url: .target_url}'
