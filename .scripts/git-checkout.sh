#!/bin/sh
git fetch --all -p
git checkout $(git branch --all | grep -v "^* " | grep -v "origin/HEAD" | sed 's/^  \(remotes\/\)\?//g' | fzy | sed 's/\(origin\/\)\?//g')
