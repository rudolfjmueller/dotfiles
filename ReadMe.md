# My dotfiles for my work machine

to restore follow the listed steps or check out the article of [atlassian](https://www.atlassian.com/git/tutorials/dotfiles).

```shell
git clone --bare git@gitlab.com:rudolfjmueller/dotfiles.git $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config checkout
config config --local status.showUntrackedFiles no
```

